# Haven-dev-test

### Get via:

* Run ``` git clone https://bitbucket.org/bitwillard/haven-dev-test.git ``` in your command line

### To run:

* Go to `haven-dev-test` clone folder 
* run ` npm i `
* run ` npm run start ` (if that doesn't start, you could try installing angular-cli on your machine)
* open your browser on `localhost:4200` or  wherever the command line says it is listening on.

### Note:
* It is very unpolished, there are parts that I have not been able to apply responiveness yet
* The best validation  it has is just the html input type validation
* Only tested in firefox
* unit tests haven't been touched since creation