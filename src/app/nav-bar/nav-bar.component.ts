import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.less','nav-bar.component.layout.less']
})
export class NavBarComponent implements OnInit {
  links = [
    {text:"Book keeping & payroll",active:false},
    {text:"Financial & GST",active:false},
    {text:"KiwiSaver advice",active:true},
    {text:"Insuring people",active:false},
    {text:"Tax & business advice",active:false},
    {text:"Trustee services",active:false},
    {text:"Mortgage advice",active:false}
  ]
  hideNavBar = true;
  constructor() { }

  ngOnInit() {
  }
  show(){
    this.hideNavBar = !this.hideNavBar;
  }

}
