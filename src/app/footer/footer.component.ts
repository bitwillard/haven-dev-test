import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.less","./footer.component.layout.less"]
})
export class FooterComponent implements OnInit {
  links = [
    "home",
    "our services",
    "about us",
    "testimonials",
    "blog",
    "contact us"
  ];
  constructor() {}

  ngOnInit() {}
}
