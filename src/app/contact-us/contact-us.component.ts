import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.less','./contact-us.component.layout.less']
})
export class ContactUsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
