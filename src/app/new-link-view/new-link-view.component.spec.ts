import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewLinkViewComponent } from './new-link-view.component';

describe('NewLinkViewComponent', () => {
  let component: NewLinkViewComponent;
  let fixture: ComponentFixture<NewLinkViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewLinkViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewLinkViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
